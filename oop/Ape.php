<?php 

require_once 'Animal.php';

class Ape extends Animal {
    // Override Constructor
    public function __construct($name) {
        parent::__construct($name, 2, "no");
    }
    
    public function yell() {
        return "Auooo";
    }
}
?>