<?php 

require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");

echo "Name : " . $sheep->get_name() . "<br>"; // "shaun"
echo "Legs : " . $sheep->get_legs() . "<br>"; // 4
echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br>"; // "no"

echo "<br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->get_name() . "<br>"; // "buduk"
echo "Legs : " . $kodok->get_legs() . "<br>"; // 4
echo "Cold Blooded : " . $kodok->get_cold_blooded() . "<br>"; // "no"
echo "Jump : " . $kodok->jump() . "<br>"; // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->get_name() . "<br>"; // "kera sakti"
echo "Legs : " . $sungokong->get_legs() . "<br>"; // 2
echo "Cold Blooded : " . $sungokong->get_cold_blooded() . "<br>"; // "no"
echo "Yell : " . $sungokong->yell() . "<br>"; // "Auooo"

?>